import Moment from 'moment';
export const formatageDate = date => new Moment(date).format("DD/MM/YYYY");
