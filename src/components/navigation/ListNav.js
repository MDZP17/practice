import React, { useState } from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer'
import Collapse from '@material-ui/core/Collapse';
import Business from '@material-ui/icons/Business';
import Note from '@material-ui/icons/Note'
import SupervisorAccount from '@material-ui/icons/SupervisorAccount'
import Search from '@material-ui/icons/Search'
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import RateReview from '@material-ui/icons/RateReview'
import Create from '@material-ui/icons/Create'
import Home from '@material-ui/icons/Home'
import Slideshow from '@material-ui/icons/Slideshow'
import Poll from '@material-ui/icons/Poll'
import EventNote from '@material-ui/icons/EventNote'
import NewReleases from '@material-ui/icons/NewReleases'
import ListAlt from '@material-ui/icons/ListAlt'
import CreateNewFolder from '@material-ui/icons/CreateNewFolder'
import ChromeReaderMode from '@material-ui/icons/ChromeReaderMode';
import Dashboard from '@material-ui/icons/Dashboard'
import { Link } from 'react-router-dom'
import { withStyles, IconButton, useTheme, AppBar, Toolbar, Typography } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import clsx from 'clsx'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import MenuIcon from '@material-ui/icons/Menu';
import ApplicationBar from './ApplicationBar';

const drawerWidth = 300;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1.4,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    nested: {
        paddingLeft: (theme.spacing(3)),
    },
    nested2: {
        paddingLeft: (theme.spacing(5))
    }
});

const ListNav = ({ classes }) => {

    const [etat, setEtat] = useState({ administrationOpen: false, referentielsOpen: false, creationOpen: false, suiviOpen: false })
    const [open, setOpen] = useState(false)
    const theme = useTheme();

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const toggleEntry = entry => {
        setEtat({
            ...etat,
            [entry]: !etat[entry]
        })
    }

    return (

        <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
            })}
            classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                }),
            }}
        >
            <div className={classes.toolbar}>
                <IconButton onClick={handleDrawerClose}>
                    {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                </IconButton>
            </div>
            <Divider />
            <ApplicationBar barClass={clsx(classes.appBar, { [classes.appBarShift]: open, })}
                buttonClass={clsx(classes.menuButton, { [classes.hide]: open })} handleDrawerOpen={handleDrawerOpen} />
            <List component="nav">

                <ListItem button className="link" component={Link} to="/">
                    <ListItemIcon>
                        <Home color='primary' />
                    </ListItemIcon>
                    <ListItemText primary="Accueil" />
                </ListItem>


                <ListItem button onClick={() => toggleEntry('administrationOpen')}>
                    <ListItemIcon>
                        <SupervisorAccount color='primary' />
                    </ListItemIcon>
                    <ListItemText primary="Administration" />
                    {etat.administrationOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={etat.administrationOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button className={`${classes.nested} link`} component={Link} to='droits' >
                            <ListItemIcon>
                                <SupervisorAccount color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Droits" />
                        </ListItem>

                        <ListItem button className={classes.nested} onClick={() => toggleEntry('referentielsOpen')}>
                            <ListItemIcon>
                                <ListAlt color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Référentiels" />
                            {etat.referentielsOpen ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={etat.referentielsOpen} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem button className={`${classes.nested2} link`} component={Link} to='/administrationPartenaires' >
                                    <ListItemIcon>
                                        <Business color='primary' />
                                    </ListItemIcon>
                                    <ListItemText primary="Partenaires" />
                                </ListItem>
                            </List>
                        </Collapse>

                        <ListItem button className={`${classes.nested} link`} component={Link} to='processusLecture'>
                            <ListItemIcon>
                                <RateReview color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Processus de lecture" />
                        </ListItem>


                        <ListItem button className={`${classes.nested} link`} component={Link} to='actualiteReseau'>
                            <ListItemIcon>
                                <NewReleases color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Actualités nationales du réseau de l'action régionale" />
                        </ListItem>

                    </List>
                </Collapse>

                <Divider />

                <ListItem button onClick={() => toggleEntry('creationOpen')}>
                    <ListItemIcon>
                        <Note color='primary' />
                    </ListItemIcon>
                    <ListItemText primary="Création / Modification" />
                    {etat.creationOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={etat.creationOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button className={`${classes.nested} link`} component={Link} to='etude'>
                            <ListItemIcon>
                                <Create color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Études sans partenaire" />
                        </ListItem>

                        <ListItem button className={`${classes.nested} link`} component={Link} to='etudePartenaire'>
                            <ListItemIcon>
                                <EventNote color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Études avec partenaire" />
                        </ListItem>

                        <ListItem button className={`${classes.nested} link`} component={Link} to='actionCE'>
                            <ListItemIcon>
                                <Slideshow color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Actions de conseil-expertise" />
                        </ListItem>

                        <ListItem button className={`${classes.nested} link`} component={Link} to='rencontrePartenaire'>
                            <ListItemIcon>
                                <CreateNewFolder color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Rencontres de partenaires" />
                        </ListItem>

                        <ListItem button className={`${classes.nested} link`} component={Link} to='psm'>
                            <ListItemIcon>
                                <Poll color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Produits sur mesure" />
                        </ListItem>

                    </List>
                </Collapse>

                <ListItem button onClick={() => toggleEntry('suiviOpen')}>
                    <ListItemIcon>
                        <Dashboard color='primary' />
                    </ListItemIcon>
                    <ListItemText primary="Restitutions" />
                    {etat.suiviOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>

                <Collapse in={etat.suiviOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button className={classes.nested} component={Link} to='restitution-publications'>
                            <ListItemIcon>
                                <ChromeReaderMode color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Études" />
                        </ListItem>

                        <ListItem button className={classes.nested} component={Link} to='restitution-ce'>
                            <ListItemIcon>
                                <ChromeReaderMode color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Actions de conseil-expertise" />
                        </ListItem>

                        {/* 
                            <Link to='conventions' className="link">
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <LibraryBooks />
                                    </ListItemIcon>
                                    <ListItemText primary="Conventions" />
                                </ListItem>
                            </Link>

                            <Link to='psm' className="link">
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <Poll />
                                    </ListItemIcon>
                                    <ListItemText primary="Produits sur mesure" />
                                </ListItem>
                            </Link>

                            <Link to='rencontrePartenaire' className="link">
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <CreateNewFolder />
                                    </ListItemIcon>
                                    <ListItemText primary="Rencontres de partenaires" />
                                </ListItem>
                            </Link> */}

                    </List>

                </Collapse>


                <Divider />


                <ListItem button className='Link' component={Link} to='recherche'>
                    <ListItemIcon>
                        <Search color='primary' />
                    </ListItemIcon>
                    <ListItemText primary="Recherche" />
                </ListItem>



            </List>
        </Drawer >

    );
}


ListNav.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListNav);